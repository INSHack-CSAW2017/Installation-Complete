# Etapes d'installations

## Installer Docker

    apt-get install \
       apt-transport-https \
       ca-certificates \
       curl \
       gnupg2 \
       software-properties-common

    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

    apt-key fingerprint 0EBFCD88

    echo "deb [arch=armhf] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
         $(lsb_release -cs) stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list

     apt-get update

     apt-get install docker-ce

## Installer les images Docker

    git clone https://gitlab.com/INSHack-CSAW2017/Docker_PLC
    cd Docker_PLC
    sudo make build -B
    cd ..
    
    git clone https://gitlab.com/INSHack-CSAW2017/Docker_PLCBuilder
    cd Docker_PLCBuilder
    sudo make build -B
    cd ..

## Installer le site

    git clone https://gitlab.com/INSHack-CSAW2017/Website-PLCControl
    cd Website-PLCControl
    sudo ./install.sh
    cd ..

## Installer les démons

    git clone https://gitlab.com/INSHack-CSAW2017/Demon-Builder
    cd Demon-Builder
    sudo ./install.sh
    cd ..

    git clone https://gitlab.com/INSHack-CSAW2017/Demon-PLCManager
    cd Demon-PLCManager
    sudo ./install.sh
    cd ..

    git clone https://gitlab.com/INSHack-CSAW2017/Demon-Mapper
    cd Demon-Mapper
    sudo ./install.sh
    cd ..